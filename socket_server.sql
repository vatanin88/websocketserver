-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Фев 15 2016 г., 10:59
-- Версия сервера: 5.5.38-log
-- Версия PHP: 5.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `socket_server`
--

-- --------------------------------------------------------

--
-- Структура таблицы `answer`
--

CREATE TABLE IF NOT EXISTS `answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `answer` text NOT NULL,
  `game` int(10) unsigned NOT NULL,
  `question` int(10) unsigned NOT NULL,
  `user` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `game` (`game`,`question`,`user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `game`
--

CREATE TABLE IF NOT EXISTS `game` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `startUser` int(10) unsigned NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('stepOne','stepTwo','stepThree','closed','end') NOT NULL DEFAULT 'stepOne',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `like`
--

CREATE TABLE IF NOT EXISTS `like` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(10) unsigned NOT NULL,
  `like_user` int(10) unsigned NOT NULL,
  `game` int(10) unsigned NOT NULL,
  `like` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`,`like_user`,`game`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `game` int(10) unsigned NOT NULL,
  `user` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `game` (`game`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` tinyint(4) NOT NULL DEFAULT '0',
  `birthday` varchar(255) NOT NULL,
  `about` text NOT NULL,
  `image1` varchar(255) NOT NULL,
  `image2` varchar(255) NOT NULL,
  `image3` varchar(255) NOT NULL,
  `image4` varchar(255) NOT NULL,
  `image5` varchar(255) NOT NULL,
  `image6` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `deviceId` varchar(255) NOT NULL,
  `isPushNotification` tinyint(1) NOT NULL DEFAULT '0',
  `discover` tinyint(1) NOT NULL,
  `distance` bigint(20) NOT NULL,
  `ageStart` tinyint(4) NOT NULL,
  `ageEnd` tinyint(4) NOT NULL,
  `fgender` tinyint(4) NOT NULL DEFAULT '0',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `userId`, `name`, `email`, `gender`, `birthday`, `about`, `image1`, `image2`, `image3`, `image4`, `image5`, `image6`, `latitude`, `longitude`, `token`, `deviceId`, `isPushNotification`, `discover`, `distance`, `ageStart`, `ageEnd`, `fgender`, `last_update`) VALUES
(1, 2147483649, 'Anton', '', 0, '', '', '154', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '2016-02-11 14:39:02'),
(2, 2, 'Тарас', 'komp-w@yandex.ru', 1, '10 октября 1997', '', '103', '', '', '', '', '', '37.33049888', '-122.02873696', '', '4C38A897-75A0-4FEF-B67C-13F2300F29F4', 1, 0, 942, 37, 82, 2, '0000-00-00 00:00:00'),
(3, 943810082353102, 'Denis', 'denis@borzenkov.ru', 0, '10 October 1997', 'Живу в Москве.', '59', '60', '61', '62', '63', '64', '59.88712095491608', '30.42266442333316', '', '41E0B06C-A434-4863-90D9-CE56AAEF7A60', 1, 1, 0, 18, 99, 1, '2016-02-04 02:56:04'),
(4, 939115056137368, 'Polina', 'p.stepanova@list.ru', 0, '28 декабря 1989', '', '58', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '2016-02-11 14:24:44'),
(5, 954273631306963, 'Inna', 'koro-inna@yandex.ru', 0, '4 сентября 1988', '', '65', '', '', '', '', '', '53.20776002209573', '50.20479948728664', '', '556A2B0D-746B-4A64-B245-B158B7381766', 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00'),
(32, 442489562620945, 'Тарас', 'komp-w@yandex.ru', 0, '10 октября 1997', 'Weight fab usages. Gsvgdcgfcvgydghtr recharged ffg facts checked fdfcg checked fdf ffg', 'http://www.w3.org/html/logo/badge/html5-badge-h-connectivity.png', 'http://www.w3.org/html/logo/badge/html5-badge-h-connectivity.png', 'http://www.w3.org/html/logo/badge/html5-badge-h-connectivity.png', 'http://www.w3.org/html/logo/badge/html5-badge-h-connectivity.png', 'http://www.w3.org/html/logo/badge/html5-badge-h-connectivity.png', 'http://www.w3.org/html/logo/badge/html5-badge-h-connectivity.png', '37.33023169', '-122.02690797', '', '629EA937-5C30-44CE-9B53-21C16140C8E3', 0, 1, 949, 39, 78, 1, '2016-02-12 14:57:12');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
