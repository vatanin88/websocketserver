<?php

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use socketServer\app;

require 'vendor/autoload.php';

set_time_limit(0);

ORM::configure('mysql:host=localhost;dbname=socket_server;charset=utf8');
ORM::configure('username', 'socket_server');
ORM::configure('password', 'server88');
ORM::configure('driver_options', array(
    PDO::ATTR_PERSISTENT => true
));

$app = new app;

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            $app
        )
    ),
    2100
);

$server->loop->addPeriodicTimer(1, function($timer) use ($app)  {
    if (!$app->workRun) {
        $app->work();   
    }
     
});

$server->run();
