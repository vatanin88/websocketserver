<?php

namespace socketServer;

class user {

    private static $questionTableName = 'question';
    private static $answerTableName = 'answer';
    private static $likeTableName = 'like';
    public $ansewrs = array();
    public $connect;
    public $model;
    public $game;
    public $question;
    public $questionId;
    public $likes = array();

    public function __construct(&$connect, $params, $model) {
        $this->connect = &$connect;
        $this->model = $model;
        $this->question = $params[1]['question'];
        game::createOrFindGame($this);
    }
    
    
    
    public function saveQuestion() {    
        \ORM::reset_db();
         $question = \ORM::for_table(self::$questionTableName)->create();
         $question->question = $this->question;
         $question->game = $this->game->model->id;
         $question->user = $this->model->id;
         $question->save();
         $this->questionId = $question->id;
    }
    
    
    public function read($params) {
        
        switch ($params[0]) {
            case 'cancelSession' : 
                $this->game->deleteUser($this);
                break;
            case 'answer' :
                $this->game->answer($this,$params);
                break;
            case 'likeUserAnswer' :
                $this->game->likeUserAnswer($this,$params);
                break;
            default :
                break;
        }
    }
    
    
    public function saveAnswerAndLikes() {
        \ORM::reset_db();
        foreach ($this->ansewrs as $id => $answer) { 
            
            if (!isset(app::$users[$id])) {
                continue;
            }
            
            $model = \ORM::for_table(self::$answerTableName)->create();
            $model->question = app::$users[$id]->questionId;
            $model->game = $this->game->model->id;
            $model->answer = $answer;
            $model->user = $this->model->id;
            $model->save();
        }
        
        foreach ($this->likes as $id => $like) {
            if (!isset(app::$users[$id])) {
                continue;
            }
            
            $model = \ORM::for_table(self::$likeTableName)->create();
            $model->game = $this->game->model->id;
            $model->user = $this->model->id;
            $model->like_user = app::$users[$id]->model->id;
            $model->like = $like;   
            $model->save();
        }
        
    }   
    
    
    public function  retryConnection($connect) {
        $this->connect->close();
        $this->connect = $connect;
        $this->game->retrySend($this);
        
    }

}
