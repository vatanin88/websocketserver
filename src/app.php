<?php

namespace socketServer;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class app implements MessageComponentInterface {
    
    private static $userTableName = "user";
    protected $clients;
    public static $users = array();
    public static $games = array();
    public $workRun = false;
    
    public static function sendMessage(ConnectionInterface $connect, $name, $params = false) {
        
        if ($params) {
            $message = $name . "::" . json_encode($params, JSON_UNESCAPED_UNICODE) . "\n";
        } else {
            $message = $name . "\n";
        }

        
        $connect->send($message);
    }

    public function __construct() {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $connect) {
        $this->clients->attach($connect);        
        logger::log("New connection! ({$connect->resourceId})", $connect, 2);
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        
        if ($params = $this->decodeParams($msg)) {
            switch ($params[0]) {
                case 'setsession':
                    $this->setsession($from, $params);
                    break;
                default :
                    foreach (app::$users as &$user) {
                        if ($from == $user->connect) {
                            $user->read($params);
                        }
                    }
                    break;
            }
        } else {
            $from->close();
        }
        
    }

    public function onClose(ConnectionInterface $connect) {
        $this->clients->detach($connect);
        logger::log("Connection {$connect->resourceId} has disconnected", $connect, 2);
    }

    public function onError(ConnectionInterface $connect, \Exception $e) {        
        logger::log("An error has occurred: {$e->getMessage()}", $connect, 1);
        $connect->close();
    }
    
    public function work() {
        $this->workRun = true;
        foreach (self::$games as &$game) {
            $game->work();
        }
        $this->workRun = false;
    }
    
    private function setsession(ConnectionInterface $connect, $params) {
        
        if (isset(app::$users[$params[1]['Session']])) {
            app::$users[$params[1]['Session']]->retryConnection($connect);
            return;
        }
        
        \ORM::reset_db();
        $model = \ORM::for_table(self::$userTableName)->where("id", $params[1]['Session'])->find_one();
        if ($model) {
            $user = new user($connect, $params, $model);
            app::$users[$user->model->id] = $user;
        } else {
            $messageArray = array(
                "nameMethod" => "setsession",
                "errorMessage" => "User {$params[1]['Session']} not found"
            );
            app::sendMessage($connect, 'error', $messageArray);
            logger::log("User {$params[1]['Session']} not found", $connect, 1);
            $connect->close();
        }
    }    

    private function decodeParams($data) {
        $message = explode("::", $data);
        if (count($message) != 2) {
            return false;
        }
        $params = json_decode(trim($message[1]), true , 512, JSON_BIGINT_AS_STRING);
        if (!$params) {
            return false;
        }

        return array($message[0], $params);
    }
    
    

}
