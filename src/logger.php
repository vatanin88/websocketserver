<?php

namespace socketServer;

class logger {
    private static $logLevel = 9;
    
    public static function log($string, $connect = false, $logLevel=9) {
        
        if ($logLevel > self::$logLevel) {
            return;
        }
        
        $address = '';
//        if ($connect) {
//            $address = stream_socket_get_name($connect, true);
//        }

        $date = date('Y-m-d H:i:s');
        echo "$date $address $string\r\n";
    }
    
}

