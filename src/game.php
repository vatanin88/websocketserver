<?php

namespace socketServer;

class game {

    private static $maxUsers = 3;
    private static $minUsers = 2;
    private static $gameTableName = 'game';
    private static $stepOneTime = 30;
    private static $stepTwoTime = 30;
    private static $stepThreeTime = 30;
    private static $sendTimeout = 0;
    private static $fileUrl = 'http://v.alt-idea.ru/post-server/';
    private $startTime;
    private $lastSend = 0;
    private $update;
    public $step;
    public $model;
    public $users = array();

    public static function createOrFindGame(&$user) {
        if (!app::$games) {
            $game = new game($user);
            app::$games[$game->model->id] = $game;
            return;
        }

        foreach (app::$games as &$game) {
            //тут обработка поиска подходящих игр            
            if (count($game->users) < self::$maxUsers && $game->step == 1) {
                $game->addUser($user);
                return;
            }
        }

        $game = new game($user);
        app::$games[$game->model->id] = $game;
        return;
    }

    public function __construct(&$user) {
        \ORM::reset_db();
        $model = \ORM::for_table(self::$gameTableName)->create();
        $model->startUser = $user->model->id;
        $model->save();
        $this->model = $model;
        $this->addUser($user);
        $this->startTime = time();
        $this->step = 1;
    }

    public function addUser(&$user) {
        $user->game = &$this;
        $this->users[$user->model->id] = &$user;
        logger::log("User #{$user->model->id} add to game #{$this->model->id}", $user->connect, 2);
        $this->update = true;
        $user->saveQuestion();
    }

    public function deleteUser(&$user) {
        $this->update = true;
        logger::log("User #{$user->model->id} disconect from game #{$this->model->id}", $user->connect, 2);
        unset($this->users[$user->model->id]);
        app::$users[$user->model->id]->connect->close();
        unset(app::$users[$user->model->id]);

        if (!count($this->users)) {
            $this->closeGame();
        }
    }

    public function answer(&$user, $params) {
        $user->ansewrs[$params[1]['userId']] = $params[1]['myAnswer'];
        //$this->update = true;        
    }

    public function likeUserAnswer(&$user, $params) {
        $user->likes[$params[1]['userId']] = $params[1]['like'];
        //$this->update = true; 
    }

    public function work() {
        switch ($this->step) {
            case 1:
                $this->stepOne();
                break;
            case 2:
                $this->stepTwo();
                break;
            case 3:
                $this->stepThree();
                break;
            case 4:
                $this->endGame();
                break;
            default :
                break;
        }
    }

    private function getUsersForStepOne() {
        $array = array();

        foreach ($this->users as &$user) {
            $array[] = array(
                'userId' => $user->model->id,
                'name' => $user->model->name,
                'gender' => $user->model->gender,
                'birthday' => $user->model->birthday,
                'image1' => self::$fileUrl. $user->model->image1,
            );
        }

        return $array;
    }

    private function stepOneSend(&$user) {
        $data = $this->getUsersForStepOne();
        $messageArray = array(
            "users" => $data
        );
        app::sendMessage($user->connect, 'preparation', $messageArray);
    }

    private function stepOne() {
        if ($this->lastSend + self::$sendTimeout > time()) {
            return;
        }

        if ($this->update) {

            foreach ($this->users as &$user) {
                $this->stepOneSend($user);
            }

            $this->update = false;
        }


        if (count($this->users) == self::$maxUsers) {
            $this->step = 2;
            $this->update = true;
            $this->startTime = time();
            logger::log("Game #{$this->model->id} step 2", false, 2);
        }

        if ($this->startTime + self::$stepOneTime < time() && count($this->users) < self::$minUsers) {
            $this->closeGame();
        } elseif ($this->startTime + self::$stepOneTime < time()) {
            $this->step = 2;
            $this->update = true;
            $this->startTime = time();
            logger::log("Game #{$this->model->id} step 2", false, 2);
        }

        $this->lastSend = time();
    }

    private function getUsersForStepTwo(&$currentUser) {
        $array = array();

        foreach ($this->users as &$user) {
            if ($currentUser->model->id == $user->model->id ) {
                continue;
            }
            
            $myAnswer = '';

            if (isset($currentUser->ansewrs[$user->model->id])) {
                $myAnswer = $currentUser->ansewrs[$user->model->id];
            }

            $array[] = array(
                'userId' => $user->model->id,
                'name' => $user->model->name,
                'gender' => $user->model->gender,
                'birthday' => $user->model->birthday,
                'image1' => self::$fileUrl.$user->model->image1,
                'image2' => self::$fileUrl.$user->model->image2,
                'image3' => self::$fileUrl.$user->model->image3,
                'image4' => self::$fileUrl.$user->model->image4,
                'image5' => self::$fileUrl.$user->model->image5,
                'image6' => self::$fileUrl.$user->model->image6,
                'question' => $user->question,
                'myAnswer' => $myAnswer
            );
        }

        return $array;
    }

    private function stepTwoSend(&$user) {
        $messageArray = array(
            "timer" => $this->startTime + self::$stepTwoTime - time(),
            "users" => $this->getUsersForStepTwo($user)
        );
        app::sendMessage($user->connect, 'answerQuestions', $messageArray);
    }

    private function stepTwo() {
        if ($this->lastSend + self::$sendTimeout > time()) {
            return;
        }

        if ($this->update) {
            \ORM::reset_db();
            $this->model->status = 'stepTwo';
            $this->model->save();
            foreach ($this->users as &$user) {
                $this->stepTwoSend($user);
            }

            $this->update = false;
        }

        if ($this->startTime + self::$stepTwoTime < time()) {
            $this->step = 3;
            $this->update = true;
            $this->startTime = time();
            logger::log("Game #{$this->model->id} step 3", false, 2);
        }

        $this->lastSend = time();
    }

    private function getUsersForStepThree(&$currentUser) {
        $array = array();

        foreach ($this->users as &$user) {
            if ($currentUser->model->id == $user->model->id ) {
                continue;
            }
            
            $answer = '';
            $myLikeToAnswer = false;  

            if (isset($user->ansewrs[$currentUser->model->id])) {
                $answer = $user->ansewrs[$currentUser->model->id];
            }
            
            if (isset($user->likes[$currentUser->model->id]) && $user->likes[$currentUser->model->id]) {
                $myLikeToAnswer = true;
            }


            $array[] = array(
                'userId' => $user->model->id,
                'name' => $user->model->name,
                'gender' => $user->model->gender,
                'birthday' => $user->model->birthday,
                'image1' => self::$fileUrl.$user->model->image1,
                'image2' => self::$fileUrl.$user->model->image2,
                'image3' => self::$fileUrl.$user->model->image3,
                'image4' => self::$fileUrl.$user->model->image4,
                'image5' => self::$fileUrl.$user->model->image5,
                'image6' => self::$fileUrl.$user->model->image6,
                'question' => $user->question,
                'answer' => $answer,
                'myLikeToAnswer'=> $myLikeToAnswer
            );
        }

        return $array;
    }

    private function stepThreeSend(&$user) {
        $messageArray = array(
            "timer" => $this->startTime + self::$stepThreeTime - time(),
            "users" => $this->getUsersForStepThree($user)
        );
        app::sendMessage($user->connect, 'choiceFavorite', $messageArray);
    }

    private function stepThree() {
        if ($this->lastSend + self::$sendTimeout > time()) {
            return;
        }

        if ($this->update) {
            \ORM::reset_db();
            $this->model->status = 'stepThree';
            $this->model->save();

            foreach ($this->users as &$user) {
                $this->stepThreeSend($user);
            }

            $this->update = false;
        }

        if ($this->startTime + self::$stepThreeTime < time()) {
            $this->step = 4;
        }

        $this->lastSend = time();
    }

    public function closeGame() {

        \ORM::reset_db();
        $this->model->status = 'closed';
        $this->model->save();

        foreach ($this->users as &$user) {
            app::sendMessage($user->connect, 'notGame');
            $user->connect->close();
            unset(app::$users[$user->model->id]);
        }

        unset(app::$games[$this->model->id]);
        logger::log("Game #{$this->model->id} close", false, 2);
    }

    private function endGame() {
        
        \ORM::reset_db();
        $this->model->status = 'end';
        $this->model->save();

        foreach ($this->users as &$user) {
            $user->saveAnswerAndLikes();
        }

        foreach ($this->users as &$user) {


            $messageArray = array(
                "gameId" => $this->model->id
            );
            app::sendMessage($user->connect, 'endGame', $messageArray);
            app::$users[$user->model->id]->connect->close();
            unset(app::$users[$user->model->id]);
        }
        unset(app::$games[$this->model->id]);
        logger::log("Game #{$this->model->id} end", false, 2);
    }

    public function retrySend(&$user) {
        switch ($this->game->step) {
            case 1:
                $this->stepOneSend($user);
                break;
            case 2:
                $this->stepTwoSend();
                break;
            case 3:
                $this->stepThreeSend();
                break;
            default :
                break;
        }
    }

}
