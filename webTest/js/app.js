var ws, currentUser, timer3, timer2;



$('#step1 button').on('click', function () {
    if (!ws) {
        ws = new WebSocket("ws://" + $('[name="socket"]').val());
        ws.onopen = function () {
            log("Connection opened...");
            currentUser = $('[name="userId"]').val();
            send('setsession', {
                "Session": currentUser,
                "question": $('[name="question"]').val()
            });
        };
        ws.onclose = function () {
            log("Connection closed...")
        };
        ws.onmessage = function (evt) {
            message(evt);
        };

    }
});

$('#step2 button').on('click', function () {
    send('answer', {
        "userId": $('[name="userIds2"]').val(),
        "myAnswer": $('[name="answer"]').val()
    });   
});

$('#step3 button').on('click', function () {
    send('likeUserAnswer', {
        "userId": $('[name="userIds3"]').val(),
        "like": 1
    });   
});

$('#fs button').on('click', function () {
    send('finishStep', {
        "step": 2
    });   
});

function log(string) {
    $('#log').append( string + "\n");
}

function send(method, params) {
    var string = method + "::" + JSON.stringify(params);
    ws.send(string);
    log( string);
}


function message(evt) {
    var string = evt.data;
    string = string.substring(0, string.length - 1);
    log( string);
    
    var data = string.split('::');
    data[1] = JSON.parse(data[1]);
    
    if (data[0] == 'answerQuestions') {
        $('[name="userIds2"] option').remove();
        timer2 = data[1].timer;
        $("#times1").html(data[1].timer);
        var interval1 = setInterval(function(){
            if (timer2>0) {
                timer2 = timer2 -1;
                $("#times1").html(timer2);  
            } else {
                clearInterval(interval1);
            }                      
        }, 1000);
        $.each(data[1].users, function(i,user){
            if (currentUser == user.userId) {
                return;
            }
            $('[name="userIds2"]').append('<option value="'+user.userId+'">'+user.name+'</option>');
        });
    } else if (data[0] == 'choiceFavorite') {
        $('[name="userIds3"] option').remove();
        timer3 = data[1].timer;
        $("#times2").html(data[1].timer);
        var interval2 = setInterval(function(){
            if (timer3>0) {
                timer3 = timer3 -1;
                $("#times2").html(timer3);  
            } else {
                clearInterval(interval2);
            }                      
        }, 1000);
        
        $.each(data[1].users, function(i,user){
            if (currentUser == user.userId) {
                return;
            }
            $('[name="userIds3"]').append('<option value="'+user.userId+'">'+user.name+'</option>');
        });
    }
    
}